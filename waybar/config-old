{
    // "layer": "top", // Waybar at top layer
    // "position": "bottom", // Waybar position (top|bottom|left|right)
    "height": 47, // Waybar height (to be removed for auto height)
    // "width": 1280, // Waybar width
    // {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {album} - 
    "spacing": 4, // Gaps between modules (4px)
    // Choose the order of the modules
    "modules-left": ["custom/menu", "custom/moc", "custom/weather", "sway/window"],
    "modules-center": ["sway/workspaces", "sway/mode"],
    "modules-right": ["pulseaudio", "network", "cpu", "custom/mem", "clock", "custom/power", "tray"],
    // Modules configuration
    // "sway/workspaces": {
    //     "disable-scroll": true,
    //     "all-outputs": true,
    //     "format": "{name}: {icon}",
    //     "format-icons": {
    //         "1": "",
    //         "2": "",
    //         "3": "",
    //         "4": "",
    //         "5": "",
    //         "urgent": "",
    //         "focused": "",
    //         "default": ""
    //     }
    // },
    "keyboard-state": {
        "numlock": true,
        "capslock": true,
        "format": "{name} {icon}",
        "format-icons": {
            "locked": "",
            "unlocked": ""
        }
    },
    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },
    "sway/scratchpad": {
        "format": "{icon} {count}",
        "show-empty": false,
        "format-icons": ["", ""],
        "tooltip": true,
        "tooltip-format": "{app}: {title}"
    },
    "mpd": {
        "format": "{stateIcon} {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ⸨{songPosition}|{queueLength}⸩ {volume}% ",
        "format-disconnected": "Disconnected ",
        "format-stopped": "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ",
        "unknown-tag": "N/A",
        "interval": 2,
        "consume-icons": {
            "on": " "
        },
        "random-icons": {
            "off": "<span color=\"#f53c3c\"></span> ",
            "on": " "
        },
        "repeat-icons": {
            "on": " "
        },
        "single-icons": {
            "on": "1 "
        },
        "state-icons": {
            "paused": "",
            "playing": ""
        },
        "tooltip-format": "MPD (connected)",
        "tooltip-format-disconnected": "MPD (disconnected)"
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "tray": {
        // "icon-size": 21,
        "spacing": 10
    },
    "clock": {
        // "timezone": "America/New_York",
        //"tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        "format-alt": "{:%d-%m-%Y}"
    },
    "cpu": {
        "format": " {usage}%",
        "tooltip": false
    },
    "memory": {
        "format": "{}% "
    },
    "temperature": {
        // "thermal-zone": 2,
        // "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
        "critical-threshold": 80,
        // "format-critical": "{temperatureC}°C {icon}",
        "format": "{temperatureC}°C {icon}",
        "format-icons": ["", "", ""]
    },
    "backlight": {
        // "device": "acpi_video1",
        "format": "{percent}% {icon}",
        "format-icons": ["", "", "", "", "", "", "", "", ""]
    },
    "battery": {
        "states": {
            // "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{capacity}% {icon}",
        "format-charging": "{capacity}% ",
        "format-plugged": "{capacity}% ",
        "format-alt": "{time} {icon}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": ["", "", "", "", ""]
    },
    "battery#bat2": {
        "bat": "BAT2"
    },
    "network": {
        // "interface": "wlp2*", // (Optional) To force the use of this interface
        "format-wifi": "{essid} ({signalStrength}%) ",
        //"format-ethernet": " {ipaddr}/{cidr}",
        "format-ethernet": " ",
        "tooltip-format": " {ifname} via {gwaddr}",
        "format-linked": " {ifname} (No IP)",
        "format-disconnected": "Disconnected ⚠",
        //"format-alt": "{ifname}: {ipaddr}/{cidr}",
        "on-click": "xfce4-terminal -e nmtui"
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{icon} {volume}% {format_source}",
        "format-bluetooth": "{icon} {volume}%  {format_source}",
        "format-bluetooth-muted": " {icon} {format_source}",
        "format-muted": " {format_source}",
        "format-source": "{volume}% ",
        "format-source-muted": "",
        "format-icons": {
            "headphone": "",
            "hands-free": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", "", ""]
        },
        "on-click": "pavucontrol"
    },
    "custom/media": {
        "format": "{icon} {}",
        "return-type": "json",
        "max-length": 40,
        "format-icons": {
            "spotify": "",
            "default": "🎜"
        },
        "escape": true,
        "exec": "$HOME/.config/waybar/mediaplayer.py 2> /dev/null" // Script in resources folder
        // "exec": "$HOME/.config/waybar/mediaplayer.py --player spotify 2> /dev/null" // Filter player based on name
    },
    "custom/mem-image": {
		"format": "",
		"format-icons": {
            "default": ""
        },
    },
    "custom/menu": {
		//"format":  "<span font='24'>  </span>",
		"format":  "<span font='21'>  </span>",
		"on-click": "~/.config/rofi2/files/launchers/type-7/launcher.sh",
		"format-icons": {
            "default": ""
        },
    },
    "custom/power": {
		"format": "<span font='16'>󰚥</span>",
		//"",
		"on-click": "~/.config/rofi/powermenu.sh",
		"tooltip": false,
		"format-icons": {
            "default": ""
        },
    },
    "custom/weather": {
    "exec": "~/.config/waybar/wittr.sh",
    "return-type": "json",
    "format": "{}",
    "tooltip": true,
    "interval": 900,
    "on-click": "xfce4-terminal -x bash -c 'curl wttr.in/Caerphilly && read' "
  },
    "custom/moc": {
    "exec": "~/.config/waybar/moc.sh",
    "return-type": "json",
    "format": "[ {} ]",
    //"tooltip": true,
    "interval": 1,
    "on-click": "mocp -G",
    "on-click-right": "xfce4-terminal -e mocp"
  },
    "custom/dots": {
		"format": "     ",
		//"on-click": "~/scripts/applauncher.sh",
		"format-icons": {
            "default": ""
        },
    },
    "custom/mem": {
		"exec": "~/.config/waybar/mem.sh",
		"return-type": "json",
		"format": "{icon}  {}",
		"format-icons": {
            "default": ""
        },
		"interval": 1,
		"on-click": "xfce4-terminal -e htop"
    }
}


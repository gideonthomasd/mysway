#!/bin/sh
# Set env variables when starting a wayland session
export WLR_NO_HARDWARE_CURSORS=1
export ANKI_WAYLAND=1
export MOZ_ENABLE_WAYLAND=1
export WLR_DRM_NO_MODIFIERS=1
# For sway
export XDG_CURRENT_DESKTOP=sway
# my own
export QT_QPA_PLATFORM=wayland
export QT_QPA_PLATFORM=xcb
export XDG_CURRENT_DESKTOP=Unity

export QT_STYLE_OVERRIDE="gtk2"
export QT_QPA_PLATFORMTHEME="gtk2"
#sleep 2
#copyq &
#sway $@

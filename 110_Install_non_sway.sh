#!/bin/bash


#### Geany neofetch
mkdir -p ~/.config/geany/colorschemes
cd colorschemes
cp -r * ~/.config/geany/colorschemes
cd ..
cp geany.conf ~/.config/geany/geany.conf

mkdir -p ~/.config/neofetch
cp config.conf ~/.config/neofetch/config.conf

#####################

mkdir -p ~/.config/rofi
cd rofi
cp -r * ~/.config/rofi
cd ..

mkdir -p ~/.config/rofi2
cd rofi2
cp -r * ~/.config/rofi2
cd ..


mkdir -p ~/.themes
mkdir -p ~/.icons/Bibata-Modern-Ice

cd Bibata-Modern-Ice
cp -r * ~/.icons/Bibata-Modern-Ice
cd ..

mkdir -p ~/.local/share/fonts

cd FiraCode
cp -r * ~/.local/share/fonts
cd ..

mkdir -p ~/.cache/wal
cd wal
cp -r * ~/.cache/wal
cd ..

cp current_wallpaper.jpg ~/.cache/current_wallpaper.jpg


cp bashrc ~/.bashrc

cp Xresources ~/.Xresources

#mkdir -p ~/scripts
#cd scripts
#chmod +x *.sh
#cp -r * ~/scripts
#cd ..
mkdir -p ~/.config/gtk-3.0
cp settings.ini ~/.config/gtk-3.0/settings.ini


######Icons & Rofi-themes###################

mkdir -p ~/.icons/BeautyLine
tar -xzvf BeautyLine.tar.gz

cd BeautyLine
cp -r * ~/.icons/BeautyLine
cd ..

tar -xzvf kora.tar.gz
tar -xzvf candy-icons.tar.gz
tar -xzvf Dracula.tar.gz
tar -xzvf Sweet-Dark.tar.gz

cp -r kora ~/.icons/kora
cp -r candy-icons ~/.icons/candy-icons
cp -r Dracula ~/.themes/Dracula
cp -r Sweet-Dark ~/.themes/Sweet-Dark

#### Music #############
cp Praise.mp3 ~/Music/Praise.mp3
mkdir -p ~/wallpapers
cp town.png ~/wallpapers

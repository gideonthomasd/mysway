#!/bin/bash

sudo apt install sway swaybg swayidle geany micro swaylock xdg-desktop-portal-wlr xwayland  -y

sudo apt install adwaita-qt alsa-utils brightnessctl fonts-dejavu fonts-firacode fonts-font-awesome fonts-ubuntu gthumb libglib2.0-bin libgtk-3-0 libgtk-4-1 libnotify-bin mako-notifier network-manager network-manager-gnome papirus-icon-theme qt5-style-plugins qt5ct waybar wob wofi x11-utils -y

sudo apt install xfce4-terminal lxtask pcmanfm thunar rofi lxappearance dmenu pulseaudio htop font-manager moc lxpolkit qt6-wayland  -y

#### Debs #######
sudo apt install gdebi -y
sudo apt install python3-requests python3-send2trash -y
sudo apt install wl-clipboard -y

#sudo dpkg -i azote_1.9.7-1~tileos_amd64.deb
sudo gdebi azote_1.9.7-1~tileos_amd64.deb
#sudo dpkg -i nwg-bar_0.1.0-5~tileos_amd64.deb
#sudo dpkg -i cliphist_0.3.1-2_amd64.deb
sudo gdebi cliphist_0.3.1-2_amd64.deb

#sudo apt install exa -y
#### Wayland ####

cd waybar
chmod +x *.sh
cd ..

mkdir -p ~/.config/waybar
mkdir -p ~/.config/sway
#mkdir -p ~/.config/nwg-bar
#mkdir -p ~/.config/foot

cd waybar
cp -r * ~/.config/waybar
cd ..


cd sway
cp -r * ~/.config/sway
cd ..


#cd nwg-bar
#cp -r * ~/.config/nwg-bar
#cd ..


#cd foot
#cp -r * ~/.config/foot
#cd ..

cp startw.sh ~/startw.sh
cp starship.toml ~/.config/starship.toml

mkdir -p ~/.config/xfce4/terminal
cp terminalrc ~/.config/xfce4/terminal/terminalrc

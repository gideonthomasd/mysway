#!/bin/bash

if pgrep -x "rofi" > /dev/null
then
 pkill rofi
else
 ~/.config/rofi/powermenu.sh
fi
 
